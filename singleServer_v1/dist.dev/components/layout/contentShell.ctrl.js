angular.module('beetleLoop')
	.controller('ContentController', ContentController);

	ContentController.$inject = ['$state', 'InitialRoute', 'UpdatedRoute'];

	function ContentController($state, InitialRoute, UpdatedRoute) {
		var vm = this;
		vm.initialRoute = InitialRoute;
		vm.updatedRoute = UpdatedRoute;

		if (vm.initialRoute === null)
			$state.go('beetleLoop.login');
	}