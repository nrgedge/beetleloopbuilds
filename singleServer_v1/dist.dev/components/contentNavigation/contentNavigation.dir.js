angular.module('beetleLoop')
    .directive('contentNavigation', contentNavigation);

    function contentNavigation() {
        var directive = {
            bindToController: {
                updatedRoute:"=",
                initialRoute:"="
            },
            controller: ContentNavigationController,
            controllerAs: 'contentNavCtrl',
            link: link,
            replace: true,
            restrict: 'E',
            scope: {},
            templateUrl: 'components/contentNavigation/contentNavigation.tpl.html'
        };

        return directive;

        function link(scope, elem, attribs) {

        }
    }

    ContentNavigationController.$inject = ['$state',
                                           'MongoRxService',
                                           'ProjectBoxService'];

    function ContentNavigationController($state, MongoRxService, ProjectBoxService) {
        // this context projection
        var vm = this;

        var projectId = vm.initialRoute.project_id;

        if (vm.updatedRoute.state === "project") {
            ProjectBoxService.getAllSites(vm.updatedRoute.id).then(function(sites) {
                if (sites)
                    vm.contentCollection = sites;
                else
                    console.log("No associated sites or bad Project ID");
            });
        } else if (vm.updatedRoute.state === "site") {
            ProjectBoxService.getAllPages(projectId, vm.updatedRoute.id).then(function(pages) {
                if (pages)
                    vm.contentCollection = pages;
                else
                    console.log("No associated pages or bad project / site ID");
            });
        } else if (false) {
            $state.go('beetleLoop.login');
        }

        vm.boxClick = function(id) {
            if (vm.updatedRoute.state === "project") {
                $state.go('beetleLoop.site',{siteId:id});
            } else if (vm.updatedRoute.state === "site") {
                $state.go('beetleLoop.page',{pageId:id});
            }
        };
    }