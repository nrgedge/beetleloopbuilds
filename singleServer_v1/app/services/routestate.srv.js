angular.module('beetleLoop.services')
	.service('RouteStateService', RouteStateService);

	RouteStateService.$inject = ['$meteor', '$q'];

	function RouteStateService($meteor, $q) {
    	var routeStateService = this,
            routeStateSubObj  = $meteor.subscribe('routeState');

        routeStateService.addRoute = function(routeObj) {
            return $meteor.call('addRoute', routeObj);
        };

        routeStateService.getRoute = function() {
            return $meteor.call('getRoute');
        };

        routeStateService.removeRoute = function() {
            return $meteor.call('removeRoute');
        };

        routeStateService.updateRouteProjectId = function(projectId) {
            return $meteor.call('updateRouteProjectId', projectId);
        };

        routeStateService.updateRouteSiteId = function(siteId) {
            return $meteor.call('updateRouteSiteId', siteId);
        };

        routeStateService.updateRoutePageId = function(pageId) {
            return $meteor.call('updateRoutePageId', userId, pageId);
        };

    }