angular.module('beetleLoop.services')
	.service('ProjectBoxService', ProjectBoxService);

	ProjectBoxService.$inject = ['$meteor', '$q'];

	function ProjectBoxService($meteor, $q) {
    	var proBoxService = this,
            _assetId,
            _pageId,
            _projectId,
            _siteId,
            _templateId;

        /* Basic In Memory Getters/Setters */

        proBoxService.getAssetId    = function() { return _assetId; };
        proBoxService.getPageId     = function() { return _pageId; };
        proBoxService.getProjectId  = function() { return _projectId; };
        proBoxService.getSiteId     = function() { return _siteId; };
        proBoxService.getTemplateId = function() { return _templateId; };

        proBoxService.setAssetId    = function(id) { _assetId    = id; };
        proBoxService.setPageId     = function(id) { _pageId     = id; };
        proBoxService.setProjectId  = function(id) { _projectId  = id; };
        proBoxService.setSiteId     = function(id) { _siteId     = id; };
        proBoxService.setTemplateId = function(id) { _templateId = id; };

        /* Meteor Method API Calls */

        /* Add Calls */
        proBoxService.addAsset = function(projectId, content) {
            return $meteor.call('addAsset', projectId, content);
        };

        proBoxService.addPage = function(projectId, content) {
            return $meteor.call('addPage', projectId, content);
        };

        proBoxService.addProject = function(content) {
            return $meteor.call('addProject', content);
        };

        proBoxService.addSite = function(projectId, content) {
            return $meteor.call('addSite', projectId, content);
        };

        proBoxService.addTemplate = function(projectId, content) {
            return $meteor.call('addTemplate', projectId, content);
        };

        /* Get Calls */
        proBoxService.getAsset = function(projectId, assetId) {
            var _targetAsset = [];

            return $meteor.call('getAssets', projectId).then(function(data) {
                if (typeof data === 'undefined' ||
                    data === null ||
                    data.length === 0) {
                    return $q.resolve(false);
                }

                _targetAsset = data.filter(function(asset) {
                    if (asset.asset_id === assetId) {
                        return true;
                    }
                });

                return $q.resolve(_targetAsset);
            });
        };

        proBoxService.getAllAssets = function(projectId) {
            return $meteor.call('getAssets', projectId);
        };

        proBoxService.getPage = function(projectId, pageId) {
            var _targetPage;

            return $meteor.call('getPages', projectId).then(function(data) {
                if (typeof data === 'undefined' ||
                    data === null ||
                    data.length === 0) {
                    return $q.resolve(false);
                }

                _targetPage = data.filter(function(page) {
                    if (page.page_id === pageId) {
                        return true;
                    }
                });

                return $q.resolve(_targetPage);
            });
        };

        // if siteId passed, return only pages associated w that site,
        // otherwise return all project pages
        proBoxService.getAllPages = function(projectId, siteId) {
            var _pageCollection;

            return $meteor.call('getPages', projectId).then(function(data) {
                if (siteId) {
                    _pageCollection = data.filter(function(page) {
                        if (page.page_site_id === siteId) {
                            return true;
                        }
                    });
                    return $q.resolve(_pageCollection);
                }


                return $q.resolve(data);
            });
        };

        proBoxService.getSite = function(projectId, siteId) {
            var _targetSite;

            return $meteor.call('getSites', projectId).then(function(data) {
                if (typeof data === 'undefined' ||
                    data === null ||
                    data.length === 0) {
                    return $q.resolve(false);
                }

                _targetSite = data.filter(function(site) {
                    if (site.site_id === siteId) {
                        return true;
                    }
                });

                return $q.resolve(_targetSite);
            });
        };

        proBoxService.getAllSites = function(projectId) {
            return $meteor.call('getSites', projectId);
        };

        proBoxService.getTemplate = function(projectId, templateId) {
            var _targetTemplate;

            return $meteor.call('getTemplates', projectId).then(function(data) {
                if (typeof data === 'undefined' ||
                    data === null ||
                    data.length === 0) {
                    return $q.resolve(false);
                }

                _targetTemplate = data.filter(function(template) {
                    if (template.template_id === templateId) {
                        return true;
                    }
                });

                return $q.resolve(_targetTemplate);
            });
        };

        proBoxService.getAllTemplates = function(projectId) {
            return $meteor.call('getTemplates', projectId);
        };

        /* Remove Calls */
        proBoxService.removeAsset = function(projectId, assetId) {
            return $meteor.call('removeAsset', projectId, assetId);
        };

        proBoxService.removePage = function(projectId, pageId) {
            return $meteor.call('removePage', projectId, pageId);
        };

        proBoxService.removeProject = function(projectId) {
            return $meteor.call('removeProject', projectId);
        };

        proBoxService.removeSite = function(projectId, siteId) {
            return $meteor.call('removeSite', projectId, siteId);
        };

        proBoxService.removeTemplate = function(projectId, templateId) {
            return $meteor.call('removeTemplate', projectId, templateId);
        };

        /* Update Calls */
        proBoxService.updateAsset = function(projectId, assetId, content) {
            return $meteor.call('updateAsset', projectId, assetId, content);
        };

        proBoxService.updatePage = function(projectId, pageId, content) {
            return $meteor.call('updatePage', projectId, pageId, content);
        };

        proBoxService.updateProject = function(projectId, content) {
            return $meteor.call('updateProject', projectId, content);
        };

        proBoxService.updateSite = function(projectId, siteId, content) {
            return $meteor.call('updateSite', projectId, siteId, content);
        };

        proBoxService.updateTemplate = function(projectId, templateId, content) {
            return $meteor.call('updateSite', projectId, templateId, content);
        };

	}