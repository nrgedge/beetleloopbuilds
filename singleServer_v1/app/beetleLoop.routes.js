angular.module('beetleLoop')
    .config(BeetleLoopConfig);

    // $inject protects against minification errors
    BeetleLoopConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

    function BeetleLoopConfig($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('beetleLoop', {
                url      : '',
                abstract : true,
                resolve: {
                    RouteStateService:"RouteStateService",
                    InitialRoute: function(RouteStateService) {
                        return RouteStateService.getRoute().then(function(data) {
                            if ( typeof data === 'undefined' || data === null ) {
                                return null;
                            } else {
                                return data;
                            }
                        });
                    }
                }
            })

            .state('beetleLoop.login', {
                url: '/',
                views: {
                  "body_content@": {
                    controller  : 'UserController as userCtrl',
                    templateUrl: 'components/users/userShell.tpl.html'
                  }
                },
                data : { pageTitle: 'Welcome to BeetleLoop!' }
            })

            .state('beetleLoop.project', {
                url: '/project/:projectId',
                views: {
                  "top_navigation@" : {
                    controller  : 'TopNavigationController as topNavCtrl',
                    templateUrl : 'components/layout/topNavigationShell.tpl.html'
                  },
                  "body_content@": {
                    controller  : 'ContentController as contentCtrl',
                    templateUrl: 'components/layout/contentShell.tpl.html'
                  }
                },
                data : { pageTitle: 'My Project' },
                resolve: {
                    ProjectBoxService:"ProjectBoxService",
                    UpdatedRoute: function($stateParams, ProjectBoxService) {
                        if ($stateParams.projectId) {
                            ProjectBoxService.setProjectId($stateParams.projectId);
                            
                            return {
                                "state":"project",
                                "id":$stateParams.projectId
                            };
                        }

                        return false;
                    }
                }
            })

            .state('beetleLoop.site', {
                url: '/project/site/:siteId',
                views: {
                  "top_navigation@" : {
                    controller  : 'TopNavigationController as topNavCtrl',
                    templateUrl : 'components/layout/topNavigationShell.tpl.html'
                  },
                  "body_content@": {
                    controller  : 'ContentController as contentCtrl',
                    templateUrl: 'components/layout/contentShell.tpl.html'
                  }
                },
                data : { pageTitle: 'My Site' },
                resolve: {
                    RouteStateService:"RouteStateService",
                    UpdatedRoute: function($stateParams, RouteStateService) {
                        if ($stateParams.siteId) {
                            return RouteStateService.updateRouteSiteId($stateParams.siteId).then(function(data) {
                                var _statusObj = {
                                    "state":"site",
                                    "id":$stateParams.siteId
                                };

                                if (data) {
                                    return _statusObj;
                                }

                                return false;
                            });
                        } else 
                            return false;
                    }
                }
            })

            .state('beetleLoop.page', {
                url: '/project/site/page/:pageId',
                views: {
                  "top_navigation@" : {
                    controller  : 'TopNavigationController as topNavCtrl',
                    templateUrl : 'components/layout/topNavigationShell.tpl.html'
                  },
                  "body_content@": {
                    controller  : 'WorkspaceController as workSpaceCtrl',
                    templateUrl: 'components/layout/workspaceShell.tpl.html'
                  }
                },
                data : { pageTitle: 'My Awesome Page Title' },
                resolve: {
                    RouteStateService:"RouteStateService",
                    UpdatedRoute: function($stateParams, RouteStateService) {
                        if ($stateParams.pageId) {
                            return RouteStateService.updateRouteSiteId($stateParams.pageId).then(function(data) {
                                var _statusObj = {
                                    "state":"page",
                                    "id":$stateParams.pageId
                                };

                                if (data) {
                                    return _statusObj;
                                }

                                return false;
                            });
                        } else
                            return false;
                    }
                }
            });

    }