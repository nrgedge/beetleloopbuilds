angular.module('beetleLoop')
	.controller('UserController', UserController);

	UserController.$inject = ['$state',
							  'RouteStateService',
							  'UserBoxService',
							  'InitialRoute'];

	function UserController($state, RouteStateService, UserBoxService, InitialRoute) {
		var vm = this;

        vm.login = function() {
        	// user authentication goes here
        	// assume user ID already generated and stored on mongoDB,
        	// that logic coming soon

        	var _userId,
        		_projectId;

        	// grab user ID for the routeState entry
			UserBoxService.getUserId().then(function(data) {
				_userId = data;

				// grab the users associated projects
				return UserBoxService.getUserProjects();
			}).then(function(data) {
				// assign the first (only) project
				_projectId = data[0];

				// no routing data found...
				if (InitialRoute === null) {
					// ... so route state created from scratch
					return RouteStateService.addRoute({
					    user_id:_userId,
					    project_id:_projectId,
					    site_id:null,
					    page_id:null
					});
				// routing state exists BUT project_id is empty, which means state might be busted...
				} else if (InitialRoute && InitialRoute.project_id === null) {
					// oops we have a site or page id, route state is busted
					if (InitialRoute.site_id !== null || InitialRoute.page_id !== null) {
						// so nuke and recreate it
						return RouteStateService.removeRoute().then(function(data) {
							if (!data) {
								console.log('Failed to remove busted route state.');
								return;
							}
							// attention: this Promise is nested a bit deeper
							return RouteStateService.addRoute({
							    user_id:_userId,
							    project_id:_projectId,
							    site_id:null,
							    page_id:null
							});
						}).then(function(data) {
							if (!data) {
								console.log('Failed to re-create new route state');
							} else {
								$state.go('beetleLoop.project',{projectId:_projectId});
							}
						});
					}
				} else {
					// pass true into final Promise chain
					return true;
				}
			}).then(function(data) {
				if (!data) {
					console.log('Failed to create new route state');
				} else {
			        if (InitialRoute.page_id !== null && InitialRoute.site_id !== null) {
			            $state.go('beetleLoop.page',{pageId:InitialRoute.page_id});
			        } else if (InitialRoute.site_id !== null) {
			            $state.go('beetleLoop.site',{siteId:InitialRoute.site_id});
			        } else {
			            $state.go('beetleLoop.project',{projectId:InitialRoute.project_id});
			        }
				}
			});
        };
	}