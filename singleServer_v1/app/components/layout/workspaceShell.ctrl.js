angular.module('beetleLoop')
	.controller('WorkspaceController', WorkspaceController);

	WorkspaceController.$inject = ['$state', 'InitialRoute', 'UpdatedRoute'];

	function WorkspaceController($state, InitialRoute, UpdatedRoute) {
		var vm = this;
		vm.initialRoute = InitialRoute;
		vm.updatedRoute = UpdatedRoute;

		if (vm.initialRoute === null)
			$state.go('beetleLoop.login');
	}