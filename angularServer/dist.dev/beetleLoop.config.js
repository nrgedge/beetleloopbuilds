angular.module('beetleLoop')
    .config(BeetleLoopConfig)
    .run(BeetleLoopRun);

    // $inject protects against minification errors
    BeetleLoopConfig.$inject  = ['localStorageServiceProvider'];    

    // $inject protects against minification errors
    BeetleLoopRun.$inject  = ['$rootScope',
                              '$state',
                              '$stateParams',
                              'ProjectBoxService'];

    function BeetleLoopConfig(localStorageServiceProvider) {
        localStorageServiceProvider
          .setPrefix('beetleLoopStorage')
          .setStorageType('localStorage')
          .setStorageCookie(1, '/');
    }

    // Dynamic page title, plus hardcoded user ID
    function BeetleLoopRun($rootScope, $state, $stateParams, ProjectBoxService) {
        $rootScope.$state       = $state;
        $rootScope.$stateParams = $stateParams;
    }