angular.module('beetleLoop')
    .directive('contentNavigation', contentNavigation);

    function contentNavigation() {
        var directive = {
            bindToController: {
                updatedRoute:"=",
                initialRoute:"="
            },
            controller: ContentNavigationController,
            controllerAs: 'contentNavCtrl',
            link: link,
            replace: true,
            restrict: 'E',
            scope: {},
            templateUrl: 'components/contentNavigation/contentNavigation.tpl.html'
        };

        return directive;

        function link(scope, elem, attribs) {

        }
    }

    ContentNavigationController.$inject = ['$state',
                                           'ProjectBoxService'];

    function ContentNavigationController($state, ProjectBoxService) {
        // this context projection
        var vm = this;

        ProjectBoxService.getPages().then(function(data) {
            vm.pageCollection = data.data;

            return ProjectBoxService.getSites();
        }).then(function(data) {

            vm.siteCollection = data.data;
        });

        if ($state.current.name === "beetleLoop.project") {
            vm.listParse = 'site';
        } else if ($state.current.name === "beetleLoop.site") {
            vm.listParse = 'page';
        }

        vm.boxClick = function(source) {
            if (source === 'site')
                $state.go('beetleLoop.site');
            else if (source === 'page') {
                $state.go('beetleLoop.page');
            }
        };
    }