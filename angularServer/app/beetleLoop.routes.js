angular.module('beetleLoop')
    .config(BeetleLoopConfig);

    // $inject protects against minification errors
    BeetleLoopConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

    function BeetleLoopConfig($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('beetleLoop', {
                url      : '',
                abstract : true
            })

            .state('beetleLoop.login', {
                url: '/',
                views: {
                  "body_content@": {
                    controller  : 'HomeController as homeCtrl',
                    templateUrl: 'components/homeShell.tpl.html'
                  }
                },
                data : { pageTitle: 'Welcome to BeetleLoop!' }
            })

            .state('beetleLoop.project', {
                //url: '/project/:projectId',
                url: '/project',
                views: {
                  "top_navigation@" : {
                    controller  : 'TopNavigationController as topNavCtrl',
                    templateUrl : 'components/layout/topNavigationShell.tpl.html'
                  },
                  "body_content@": {
                    controller  : 'ContentController as contentCtrl',
                    templateUrl: 'components/layout/contentShell.tpl.html'
                  }
                },
                data : { pageTitle: 'My Project' }
            })

            .state('beetleLoop.site', {
                //url: '/project/site/:siteId',
                url: '/project/site',
                views: {
                  "top_navigation@" : {
                    controller  : 'TopNavigationController as topNavCtrl',
                    templateUrl : 'components/layout/topNavigationShell.tpl.html'
                  },
                  "body_content@": {
                    controller  : 'ContentController as contentCtrl',
                    templateUrl: 'components/layout/contentShell.tpl.html'
                  }
                },
                data : { pageTitle: 'My Site' }
            })

            .state('beetleLoop.page', {
                //url: '/project/site/page/:pageId',
                url: '/project/site/page',
                views: {
                  "top_navigation@" : {
                    controller  : 'TopNavigationController as topNavCtrl',
                    templateUrl : 'components/layout/topNavigationShell.tpl.html'
                  },
                  "body_content@": {
                    controller  : 'WorkspaceController as workSpaceCtrl',
                    templateUrl: 'components/layout/workspaceShell.tpl.html'
                  }
                },
                data : { pageTitle: 'My Awesome Page Title' }
            });

    }