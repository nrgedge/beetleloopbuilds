angular.module('beetleLoop.services')
	.service('StorageService', StorageService);

	StorageService.$inject = ['localStorageService'];

	function StorageService(localStorageService) {
    	var storageService = this;

      // returns Boolean
      storageService.setPageContent = function(key, value) {
        return localStorageService.set(key, value);
      };

      // returns value from local storage
      storageService.getPageContent = function(key) {
        return localStorageService.get(key);
      };

      // returns array of keys
      storageService.getKeyList = function() {
        return localStorageService.keys();
      };

      // returns Boolean
      storageService.removePageContent = function(key) {
        return localStorageService.remove(key);
      };

      // returns Boolean
      storageService.clearDatabase = function() {
        return localStorageService.clearAll();
      };

	}