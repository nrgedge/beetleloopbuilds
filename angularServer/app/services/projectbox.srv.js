angular.module('beetleLoop.services')
	.service('ProjectBoxService', ProjectBoxService);

	ProjectBoxService.$inject = ['$http'];

	function ProjectBoxService($http) {
    	var proBoxService = this;

        proBoxService.getPages = function() {
            return $http({
              method: 'GET',
              url: 'http://localhost:3000/pages'
            });
        };

        proBoxService.getSites = function() {
            return $http({
              method: 'GET',
              url: 'http://localhost:3000/sites'
            });
        };

	}