angular.module('beetleLoop', [
	'beetleLoop.services',

	'ui.router',
	'rx',
	'LocalStorageModule',
    'ui.ace',
    'ngFile',
    'base64'
]);