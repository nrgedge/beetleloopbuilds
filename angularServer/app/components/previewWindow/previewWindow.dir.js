angular.module('beetleLoop')
    .directive('previewWindow', previewWindow);

    previewWindow.$inject = ['$sce', '$rootScope', 'StorageService'];

    function previewWindow($sce, $rootScope, StorageService) {
        var directive = {
            bindToController: {
                content:"="
            },
            controller: PreviewWindowController,
            controllerAs: 'previewCtrl',
            link: link,
            replace: true,
            restrict: 'E',
            scope: {},
            templateUrl: 'components/previewWindow/previewWindow.tpl.html'
        };

        return directive;

        function link(scope, elem, attribs, ctrl) {
            var previewArea = angular.element(elem);

            previewArea.on('click', function(evt) {
                var target = angular.element(evt.target);

                if (target.attr('data-magnolia-area')) {
                    target.removeAttr('data-magnolia-area');
                } else {
                    target.attr('data-magnolia-area', true);
                }

                target.toggleClass("bl-ui-outline");

            });

            // Update Preview Window when update detected
            function updatePreviewWindowContent(newDocument) {
                if (typeof newDocument !== "undefined") {
                    ctrl.previewWindowContent = $sce.trustAsHtml($rootScope.codeEditorContent);
                }
            }


        }
    }

    PreviewWindowController.$inject = ['$sce',
                                       '$rootScope',
                                       'ProjectBoxService',
                                       'observeOnScope',
                                       'StorageService'];

    function PreviewWindowController($sce, $rootScope, ProjectBoxService, observeOnScope, StorageService) {

        // this context projection
        var vm = this,
            _initialContent = StorageService.getPageContent('user01');

        if (_initialContent !== null)
            vm.previewWindowContent = $sce.trustAsHtml(_initialContent.join('\n'));

        // observe $rootScope for changes to the codeEditorContent property, and subscribe and event handler
        observeOnScope($rootScope, 'codeEditorContent').subscribe(function(change) {
            if (typeof change.newValue !== 'undefined')
                vm.previewWindowContent = $sce.trustAsHtml(pushIntoView(change));
        });

        function pushIntoView(value) {
            return value.newValue.join('\n');
        }

    }