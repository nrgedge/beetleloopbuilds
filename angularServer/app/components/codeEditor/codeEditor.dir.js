angular.module('beetleLoop')
    .directive('codeEditor', codeEditor);

    codeEditor.$inject = [];

    function codeEditor() {
        var directive = {
            bindToController: {
                content:"="
            },
            controller: CodeEditorController,
            controllerAs: 'codeEditCtrl',
            link: link,
            replace: true,
            restrict: 'E',
            scope: {},
            templateUrl: 'components/codeEditor/codeEditor.tpl.html'
        };

        return directive;

        function link(scope, elem, attribs, ctrl) {

        }
    }

    CodeEditorController.$inject = ['$timeout',
                                    '$rootScope',
                                    'ProjectBoxService',
                                    'StorageService'];

    function CodeEditorController($timeout, $rootScope, ProjectBoxService, StorageService) {

        // this context projection
        var vm = this,
            _initialContent = StorageService.getPageContent('user01');

        // Instantiate and configure Ace Editor
        vm.projectBoxLoaded = function(lcEditor){
            vm.aceEditor = lcEditor;

            // insert first content
            if (_initialContent !== null)
                lcEditor.insert(_initialContent.join('\n'));

            // blocks irritating message in console
            lcEditor.$blockScrolling = Infinity;
            
            // Tease apart session, document, and renderer
            var lcSession = lcEditor.getSession(),
                lcDocument = lcSession.getDocument(),
                lcRenderer = lcEditor.renderer;

            // Configure Editor
            lcEditor.setReadOnly(false);
            lcEditor.setTheme("ace/theme/twilight");

            // Configure Session
            lcSession.setMode('ace/mode/html');
            lcSession.setUndoManager(new ace.UndoManager());

            // Configure Renderer
            lcRenderer.setShowGutter(true);

            // Events
            lcSession.on("change", function(e){
                if (lcEditor.isFocused() === true) {
                    $timeout.cancel(vm.startTimer);

                    vm.startTimer = $timeout(function(){
                        updateDatabase(lcDocument.getAllLines());
                    }, 1000);
                }
            });

        };

        function updateDatabase(contentArray) {
            vm.templateArray = extractContent(contentArray);
            // place raw contentArray into the codeEditorContent property of $rootScope to make it
            // available to other components
            $rootScope.codeEditorContent = contentArray;

            // save it into our super awesome database
            StorageService.setPageContent('user01', contentArray);
        }

        function extractContent(contentArray) {
            return contentArray.map(function(member) {
                return purgedLine(member);
            });
        }

        function purgedLine(member) {
            var _memberArray = member.split(''),
                _garbageFlag = true,
                _purgedArray = [];

            _memberArray.map(function(character) {
                if (character === "<") {
                    _purgedArray.push(character);
                    _garbageFlag = false;
                } else if (character === ">") {
                    _purgedArray.push(character);
                    _garbageFlag = true;    
                } else if (!_garbageFlag) {
                    _purgedArray.push(character);
                }
            });

            return _purgedArray.join('');
        }
    }