angular.module('beetleLoop')
    .directive('fileUpload', fileUpload);

    function fileUpload () {
        var directive = {
            controller: FileUploadController,
            controllerAs: 'fileUpCtrl',
            link: link,
            replace: true,
            restrict: 'E',
            scope: {},
            templateUrl: 'components/fileUpload/fileUpload.tpl.html'
        };

        return directive;

        function link(scope, elem, attribs, ctrl) {

        }
    }

    FileUploadController.$inject = ['$base64',
                                    '$window',
                                    'ProjectBoxService'];

    function FileUploadController($base64, $window, ProjectBoxService) {

        // this context projection
        var vm = this;


    }