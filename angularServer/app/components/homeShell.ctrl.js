angular.module('beetleLoop')
	.controller('HomeController', HomeController);

	HomeController.$inject = ['$state'];

	function HomeController($state) {
		var vm = this;

        vm.login = function() {
        	$state.go('beetleLoop.project');
        };
	}