Meteor.publish("routeState", function() {
	var currentUserId = Meteor.call("getUserId");
	
	return RouteState.find({user_id: currentUserId});
});